/*output information about the power, memory patterns, and register flow of the Freedom E310 development board. It achieves this by reading the values of certain internal registers and printing them to the console.*/


#include "platform.h"
#include "encoding.h"
#include <stdio.h>

// Function to read and print power-related information
void print_power_info() {
    // Read the VMCOREVDD and VMCOREVDDH registers to get the current voltage
    uint32_t vmcorevdd = read_csr(0xc05);
    uint32_t vmcorevddh = read_csr(0xc06);
    uint32_t voltage = (vmcorevdd & 0xff) | ((vmcorevddh & 0x1f) << 8);
    printf("Current voltage: %d mV\n", voltage);

    // Read the PCCR register to get the current clock frequency
    uint32_t pccr = read_csr(0xc04);
    uint32_t frequency = (pccr >> 28) & 0xf;
    if (frequency == 0) {
        printf("Current frequency: 16.67 MHz\n");
    } else if (frequency == 1) {
        printf("Current frequency: 8.33 MHz\n");
    } else if (frequency == 2) {
        printf("Current frequency: 4.16 MHz\n");
    } else {
        printf("Unknown frequency\n");
    }
}

// Function to read and print memory pattern information
void print_memory_info() {
    // Read the MCER register to get memory error information
    uint32_t mcer = read_csr(0x342);
    if (mcer != 0) {
        printf("Memory error detected! MCER value: %x\n", mcer);
    }

    // Read the MHPMCOUNTER3 register to get the number of cache hits
    uint32_t mhpmcounter3 = read_csr(0xb03);
    printf("Cache hits: %d\n", mhpmcounter3);

    // Read the MHPMCOUNTER4 register to get the number of cache misses
    uint32_t mhpmcounter4 = read_csr(0xb04);
    printf("Cache misses: %d\n", mhpmcounter4);
}

// Function to read and print register flow information
void print_register_flow_info() {
    // Read the DPC register to get the number of pipeline stalls due to data dependencies
    uint32_t dpc = read_csr(0x7b2);
    printf("Pipeline stalls due to data dependencies: %d\n", dpc);

    // Read the MHPMCOUNTER5 register to get the number of instructions executed
    uint32_t mhpmcounter5 = read_csr(0xb05);
    printf("Instructions executed: %d\n", mhpmcounter5);
}

int main() {
    // Initialize the board
    init_platform();

    // Read and print power-related information
    print_power_info();

    // Read and print memory pattern information
    print_memory_info();

    // Read and print register flow information
    print_register_flow_info();

    // Clean up and exit
    cleanup_platform();
    return 0;
}

