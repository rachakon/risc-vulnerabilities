/** I wrote this program to evaluate RISC-V based cores against voltage fault injection (VFI) and to implement an optimized voltage-FI parameters selection algorithm. The program has three functions that will help achieve these goals.

The first function is detect_voltage_glitches(). It enables GPIO pins for glitch detection and continuously checks for voltage glitches. If a glitch is detected, it logs information about the glitch, including the time at which it occurred and the value of the glitch.

The second function is inject_voltage_fault(). It takes two parameters: the voltage level to inject and the duration of the injection in cycles. This function sets the specified voltage level on the GPIO pins and waits for the specified number of cycles.

The third function is optimize_voltage_fi_parameters(). This function is not yet implemented, and it will be used to optimize the voltage-FI parameters selection algorithm. This function will be implemented later.

In the main function, I call the detect_voltage_glitches() function to continuously monitor for voltage glitches. I also call the inject_voltage_fault() function to perform a voltage fault injection by setting the GPIO pins to a specified voltage level for a specified duration. Finally, I call the optimize_voltage_fi_parameters() function to optimize the voltage-FI parameters selection algorithm.*/



#include "platform.h"
#include "encoding.h"
#include <stdlib.h>
#include <stdio.h>

#define GPIO_BASE_ADDR 0x10012000

// GPIO pointers
volatile uint32_t* const gpio_out = (uint32_t*)(GPIO_BASE_ADDR + 0x08);
volatile uint32_t* const gpio_in  = (uint32_t*)(GPIO_BASE_ADDR + 0x0C);

// Function to detect voltage glitches
void detect_voltage_glitches() {
    // Enable GPIO pins for glitch detection
    GPIO_REG(GPIO_INPUT_EN)  = 0xFFFFFFFF;
    GPIO_REG(GPIO_OUTPUT_EN) = 0xFFFFFFFF;
    GPIO_REG(GPIO_OUTPUT_VAL) = 0x00000000;
    
    while(1) {
        // Detect voltage glitches
        if(*gpio_in != 0xFFFFFFFF) {
            // Log information about the glitch
            printf("Voltage glitch detected at time %d with value %x\n", read_cycle(), *gpio_in);
        }
    }
}

// Function to perform voltage fault injection
void inject_voltage_fault(uint32_t voltage_level, uint32_t duration_cycles) {
    // Set the voltage level on the GPIO pins
    *gpio_out = voltage_level;
    
    // Wait for the specified number of cycles
    for(uint32_t i=0; i<duration_cycles; i++);
}

// Function to optimize voltage-FI parameters selection algorithm
void optimize_voltage_fi_parameters() {
    // TODO: Implement voltage-FI parameters selection algorithm
}

int main() {
    // Detect voltage glitches
    detect_voltage_glitches();
    
    // Perform voltage fault injection
    inject_voltage_fault(0x00000001, 10000);
    
    // Optimize voltage-FI parameters selection algorithm
    optimize_voltage_fi_parameters();
    
    return 0;
}

