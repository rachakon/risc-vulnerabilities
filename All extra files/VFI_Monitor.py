import chipwhisperer as cw

# Set up connection to the target device
scope = cw.scope()
target = cw.target(scope)

# Select the voltage glitch module
glitch = cw.glitch_module(scope)
glitch.set_target(target)

# Set the glitch parameters for voltage glitching
glitch.repeat = 1
glitch.width = 6
glitch.offset = -50  # adjust this value to change the glitch offset
glitch.ext_offset = 0
glitch.trigger_src = 'manual'
glitch.output = 'glitch_only'
glitch.arm()

# Set up the instruction measurement module
measure = cw.measurements.Module(scope)
measure.add_measurement("instruction")
measure.add_trigger_function(glitch.glitch)
measure.set_num_measurements(5000)

# Arm the scope and target
scope.arm()
target.flush()

# Run the glitch attack and capture instruction output
measure.run()
results = measure.get_measurement("instruction")

# Check the instruction output for any unexpected or incorrect results
print(results)
