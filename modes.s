// Include necessary files
.include "./inc/macros.inc"
.include "./inc/mmap.inc"
.include "./inc/clint.inc"
.include "./inc/csr.inc"

// Define UART constants
.equ UART_BASE_ADDR, 0x10013000
.equ UART_TXDATA, 0x00
.equ UART_TXFULL, 0x0c

.text
.section .text

.globl my_app_entry
my_app_entry:
    // Load the UART base address to t0
    la t0, UART_BASE_ADDR
    // Load ASCII value for 'M' to t1
    li t1, 'M'
    // Write the value to the UART data register
    sw t1, UART_TXDATA(t0)
    // infinite loop
    j my_app_entry

.globl my_user_mode_func
my_user_mode_func:
    // Load the UART base address to t0
    la t0, UART_BASE_ADDR
    // Load the ASCII value for 'U' to t1
    li t1, 'U'
    // Write the value to the UART data register
    sw t1, UART_TXDATA(t0)
    // infinite loop
    j my_user_mode_func

.globl my_priv_mode_func
my_priv_mode_func:
    // Load the UART base address to t0
    la t0, UART_BASE_ADDR
    // Load the ASCII value for 'P' to t1
    li t1, 'P'
    // Write the value to the UART data register
    sw t1, UART_TXDATA(t0)
    // infinite loop
    j my_priv_mode_func
