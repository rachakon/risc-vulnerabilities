import chipwhisperer as cw
import numpy as np
import time
import matplotlib.pyplot as plt
import pandas as pd
from multiprocessing import Pool

class Glitcher:
    def __init__(self):
        self.scope = cw.scope()
        self.target = cw.target(self.scope)
        self.glitch = cw.glitch.GlitchController(self.scope, self.target)

        self.measure = cw.measurements.Measurement(self.scope, "power")
        self.measure.set_trigger_func(self.glitch.glitch)
        self.measure.set_sample_count(5000)

        self.params = {
            'repeat': 1,
            'width': 0.5,
            'offset': 0.1,
            'ext_offset': 10,
            'trigger_src': 'clk_xor',
            'output': 'clock_xor'
        }

    def run_glitch_attack(self, **kwargs):
        self.params.update(kwargs)

        for k, v in self.params.items():
            setattr(self.glitch, k, v)

        self.glitch.arm()
        self.scope.arm()
        self.target.flush()

        self.measure.run()
        results = self.measure.fetch_measurement()

        reg_value = self.target.read_register(0)
        print(f"Value of register 0: {reg_value}")

        results_df = pd.DataFrame(results)
        results_df.to_csv('results_run_glitch_attack.csv', index=False)

        return results

    def optimize_glitch_parameters(self):
        optimal_params = self.params.copy()
        optimal_results = np.inf

        repeats = range(1, 10)
        widths = offsets = ext_offsets = np.linspace(0, 1, 10)

        pool = Pool(processes=4)  # Adjust based on your hardware

        for repeat in repeats:
            for width in widths:
                for offset in offsets:
                    for ext_offset in ext_offsets:
                        results = pool.apply_async(self.run_glitch_attack, kwargs={'repeat':repeat, 'width':width, 'offset':offset, 'ext_offset':ext_offset})
                        results = results.get()
                        if results < optimal_results:
                            optimal_results = results
                            optimal_params = {
                                'repeat': repeat,
                                'width': width,
                                'offset': offset,
                                'ext_offset': ext_offset,
                            }

        pool.close()
        pool.join()

        self.params = optimal_params
        print(f"Optimized parameters: {self.params}")

        results_df = pd.DataFrame(self.params, index=[0])
        results_df.to_csv('results_optimize_glitch_parameters.csv', index=False)

    def continuously_run(self, interval=5):
        while True:
            self.run_glitch_attack(**self.params)
            time.sleep(interval)

    def plot_results(self, results):
        plt.plot(results)
        plt.xlabel('Measurement Index')
        plt.ylabel('Power Measurement')
        plt.title('Power Measurements over Time')
        plt.savefig('power_measurements.png')

glitcher = Glitcher()
glitcher.run_glitch_attack()
glitcher.optimize_glitch_parameters()
glitcher.continuously_run()
glitcher.plot_results()
