# Voltage Fault Injection Attack Project 

## Project Overview
This project aims to perform Voltage Fault Injection (VFI) attacks on the RISC-V microcontroller to identify potential vulnerabilities. This is done by manipulating the voltage supply during the execution of specific instructions, causing unexpected behavior or even altering the outcome of computations.

The project uses the HiFive1 development board and the ChipWhisperer Lite (CW1173) platform for carrying out VFI attacks and monitoring their effects. 

## Key Components

### 1. Hardware Setup
- **HiFive1**: A development board based on the RISC-V architecture.
- **ChipWhisperer-Lite (CW1173)**: An open-source tool for hardware security research. It has features for power analysis and glitch injection attacks.

### 2. Software Components
The project consists of three main scripts:

#### 2.1 FAULTfreedomprogram.py: This Python script primarily focuses on performing glitch attacks on a target hardware (often embedded systems or IoT devices). It utilizes the ChipWhisperer library to interface with the hardware target. Specifically, it sends various sequences of electrical signals (glitches) to the target system in order to potentially disrupt its normal operation and exploit vulnerabilities.

The script creates a Glitcher object which interfaces with the hardware through ChipWhisperer and sets up initial parameters for the glitch attack. It has a run_glitch_attack method that performs a single glitch attack using the currently set parameters and collects the corresponding power measurements, saving these into a CSV file.

The optimize_glitch_parameters method is designed to find the best parameters for successful glitch attacks. It systematically varies the parameters (e.g., glitch width, offset, etc.) and stores the results of each run to eventually find the optimal glitch parameters. Once it finds the optimal parameters, it saves these to another CSV file.

The continuously_run method allows for a continuous glitch attack, running the attack at a set interval and updating the parameters according to the optimized parameters found earlier.

The plot_results method is used for visualizing the power measurements in a graphical manner, to provide insights into how the power varies during the attack.

#### 2.2 monitorCW.py: This Python script also uses the ChipWhisperer library but its purpose is different: it monitors the effects of a glitch attack. It establishes a connection with the target hardware and sets up a glitch attack with fixed parameters.

The run_glitch_attack method in this script triggers a glitch attack, collects instruction outputs during the attack, and saves these into a CSV file. It also visualizes the instruction outputs during the glitch attack on a graph. The difference with the first script is that it does not vary the parameters; it instead focuses on monitoring the results of a fixed-parameter attack.

#### 2.3newvictim.c: This C script is intended to be run on the target hardware. It simulates changes in voltage levels and duration cycles, with the goal of mimicking the behavior of a real system under a glitch attack. The code contains some GPIO (General Purpose Input/Output) operations that communicate with the Python script via hardware pins.

The adjust_voltage_and_wait function simulates an induced fault by waiting for a signal from the Python script. Once the signal is received, it waits for a certain number of cycles, simulating the delay that might be introduced by a real voltage fault.

The detect_voltage_glitches function checks for changes in input signals and prints a message if a possible voltage glitch is detected.

The inject_voltage_fault function uses the previously defined function to simulate a voltage fault with given parameters.

The optimize_voltage_fi_parameters function introduces a randomization aspect in the voltage level and duration, mimicking the randomness in a real world scenario.

The main function initializes voltage level and duration cycles parameters, optimizes them, simulates an injection of a voltage fault, and then detects voltage glitches.



## How it Works
1. The `FAULTfreedomprogram.py` script begins by setting up the hardware and initiating a glitch attack with a set of parameters. 
2. The glitch attack involves adjusting the supply voltage to the target device at specific intervals, causing a temporary glitch.
3. The script measures the power usage during the glitch attack and saves the results to a CSV file.
4. Concurrently, the `monitorCW.py` script is used to monitor the target device during the attack and record the instruction output.
5. The `newvictim.C` script running on the HiFive1 board interacts with the Python script, adjusting voltage levels and simulating a delay based on parameters received. It also detects voltage glitches and logs them.

## Observing the Results
Results are observed in a variety of ways:
1. Power measurements and instruction output during the glitch attacks are saved in CSV files by the Python scripts.
2. The `newvictim.C` script running on the HiFive1 board prints a message every time it detects a voltage glitch.
3. The `FAULTfreedomprogram.py` script plots the power measurements over time and saves the plot as a PNG image.

Please note: The CSV files and PNG images should be located in the same directory as the Python scripts. Open them to view the results of the glitch attacks.

## Hardware Connections
1. Connect the HiFive1 board to your computer via USB.
2. Connect the ChipWhisperer-Lite to your computer via USB.
3. Connect the ChipWhisperer-Lite to the HiFive1 board. Ensure that the power, ground, and GPIO pins are correctly connected according to the HiFive1 and ChipWhisperer-Lite specifications. The GPIO pins are used for communication between the Python script and the `newvictim.C` script running on the HiFive1 board.

## Running the Attack
1. Flash the `newvictim.C` program

 to the HiFive1 board.
2. Run the `FAULTfreedomprogram.py` script to initiate the glitch attack and record power measurements.
3. Run the `monitorCW.py` script to monitor the instruction output during the glitch attack.

## Disclaimer
The purpose of this project is to conduct research and study potential vulnerabilities. It is essential to ensure that such attacks are conducted in a controlled environment and within legal and ethical boundaries. Please refrain from using the information provided here to exploit or harm any systems that you do not have permission to test.