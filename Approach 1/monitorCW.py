import chipwhisperer as cw
import matplotlib.pyplot as plt
import pandas as pd

class GlitchMonitor:
    def __init__(self):
        self.scope = cw.scope()
        self.target = cw.target(self.scope)
        self.glitch = cw.glitch.GlitchController(self.scope, self.target)

    def run_glitch_attack(self):
        self.glitch.set_phase_offset(-50)
        self.glitch.set_ext_offset(0)
        self.glitch.set_repeat(1)
        self.glitch.set_output('glitch_only')
        self.glitch.set_trigger_src('manual')

        self.measure = cw.measurements.Measurements(self.scope)
        self.measure.set_measurement("instruction")
        self.measure.set_num_samples(5000)

        self.scope.arm()
        self.target.flush()

        self.measure.run()
        results_instruction = self.measure.get_measurement()

        results_instruction_df = pd.DataFrame(results_instruction)
        results_instruction_df.to_csv('results_instruction_run_glitch_attack.csv', index=False)

        plt.figure()
        plt.plot(results_instruction)
        plt.xlabel('Measurement Index')
        plt.ylabel('Instruction Output')
        plt.title('Instruction Output during Glitch Attack')
        plt.savefig('instruction_output.png')

if __name__ == '__main__':
    monitor = GlitchMonitor()
    monitor.run_glitch_attack()
