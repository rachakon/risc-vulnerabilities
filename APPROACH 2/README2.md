Sure, here is a brief README file explaining what you're asking for:

---

# Voltage Glitching Attack

## Overview

In this project, we are implementing a **Voltage Glitching Attack** on a target device running a password check program. The attack aims to introduce unexpected behavior in the target device that would allow unauthorized access.

The attack involves suddenly dropping the supply voltage to the target for a very brief period of time, hoping that it would cause the microprocessor to make a mistake. We would time this to occur during a security check operation in our target program.

## Hardware

The hardware involved in this setup includes a **HiFive1 ** as the target device and a **ChipWhisperer-Lite (CW1173)** as the glitching device.

The HiFive1 is connected to the ChipWhisperer-Lite's glitch output port through GPIO 4.

## Software

The software setup includes two programs:

1. `victim.c` - This is the target program that runs on the HiFive1 device. It is a simple program that hashes a pre-defined incorrect password and checks if it matches a stored password hash. During this check operation, it signals the ChipWhisperer that it's ready for a fault (i.e., a voltage glitch).

2. `attack.py` - This is the glitching script that runs on a connected computer. It controls the ChipWhisperer-Lite device to introduce voltage glitches in the power supply to the HiFive1. The glitches are timed according to the signal received from the `victim.c` program. The script also includes data visualization features to plot the success and failure of the glitches.

## Results

The results of the attack are presented in the terminal and through a plot showing the success and failure of the glitches based on their width and offset. A successful glitch is when the password check operation in `victim.c` is bypassed due to the introduced voltage glitch. The results are also saved to a `.cwp` file for further analysis.

To view the results, run the `attack.py` script after uploading and running the `victim.c` program on the HiFive1. Successful glitches will be displayed in the terminal and on the plot as green points, while unsuccessful glitches will be displayed as red points.

---

