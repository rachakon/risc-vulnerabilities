#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <openssl/sha.h>  // for SHA-256

#define GPIO_BASE_ADDR 0x10012000
#define PASSWORD_DELAY 3

// GPIO pointers
volatile uint32_t* const gpio_out = (uint32_t*)(GPIO_BASE_ADDR + 0x08);
volatile uint32_t* const gpio_in  = (uint32_t*)(GPIO_BASE_ADDR + 0x0C);

char hashed_password[SHA256_DIGEST_LENGTH];
char entered_password[16];

void hash_and_check_password() {
    // Hash the entered password
    unsigned char hashed_entered_password[SHA256_DIGEST_LENGTH];
    SHA256((unsigned char*)entered_password, strlen(entered_password), hashed_entered_password);

    // Signal to Python that we're ready for a fault
    *gpio_out = 0x1;

    if (memcmp(hashed_entered_password, hashed_password, SHA256_DIGEST_LENGTH) == 0) {
        printf("Access granted.\n");
    } else {
        printf("Access denied.\n");
    }

    // Wait for Python to acknowledge
    while ((*gpio_in & 0x1) == 0) {}

    // Wait a bit before accepting the next password
    sleep(PASSWORD_DELAY);
}

int main() {
    int attempts = 1000;  // Number of attempts to make
    strcpy(entered_password, "wrong_password");  // Set an incorrect password

    // Hash the correct password
    SHA256((unsigned char*)"correct_password", strlen("correct_password"), (unsigned char*)hashed_password);

    for(int i = 0; i < attempts; i++) {
        printf("Attempt #%d\n", i);
        hash_and_check_password();
    }

    return 0;
}
